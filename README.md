# champnav
### Responsive Menu for Hubspot

> Just add champnav.js and config it as follows. CSS is also provided but it is not necessary

### CDN

~~~~
<link href="https://rawgit.com/dhruvangg/champnav/master/ChampNav.css" rel="stylesheet" />
<script src="https://rawgit.com/dhruvangg/champnav/master/ChampNav.js"></script>
~~~~

### Configuration

~~~~
$(document).ready( function() {
  $(SELECTOR).ChampNavigation();
});

$(window).resize( function() {
  $(SELECTOR).ChampNavigation();
});
~~~~


### Options

~~~~
$(document).ready( function() {
  $(SELECTOR).ChampNavigation({
      breakPoint: 1024,
      button: <HERE You CAN ADD YOUR HTML>
  });
});
~~~~

| Options         |Default                        |
|----------------|-------------------------------|
|breakPoint      |1024px                         |
|button          |Humburger Icon Set             |



