$.fn.ChampNavigation = function (options) {
    var settings = $.extend({
        container: $(this),
        breakPoint: 1024,
        button: '<button class="hamburger hamburger--squeeze" type="button"><span class="hamburger-box"><span class="hamburger-inner"></span></span></button>',
    }, options);

    var button = settings.button;
    var icon = settings.icon;
    var element = settings.container;
    var breakPoint = settings.breakPoint;



    var ChampMenu = function () {
        if (!element.hasClass('ChampMenu')) {
            element.addClass("ChampMenu");
            element.removeClass("RegularMenu");
            element.before('<a href="javascript:void(0);" class="champMenu main">' + button + '</a>');
            element.hide();
            var childMenus = element.find('ul');
            childMenus.hide();
            childMenus.before('<a class="champMenu icon"><i class="icon-new">&#10095</i></a>');
            $(".champMenu").click(function () {
                var mainbutton = $(this);
                if (!mainbutton.hasClass('active')) {
                    mainbutton.addClass('active');
                } else {
                    mainbutton.removeClass('active');
                }
                $(this).next('ul').slideToggle(function () {
                    if (mainbutton.hasClass('main')) {
                        mainbutton.removeClass('main');
                        mainbutton.addClass('closed');
                    }
                    if (mainbutton.hasClass('closed')) {
                        mainbutton.removeClass('closed');
                        mainbutton.addClass('main');
                    }
                });
                mainbutton.find("button").toggleClass("is-active");
            });
        }
    }

    var RegularMenu = function () {
        element.addClass("RegularMenu");
        element.removeClass("ChampMenu");
        $(".champMenu").remove();
        element.show();
        var childMenus = element.find('ul');
        childMenus.show();
    }

    if (window.innerWidth <= breakPoint) {
        ChampMenu();
    } else {
        RegularMenu();
    }
}
